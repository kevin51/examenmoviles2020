package edu.bo.examen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    lateinit var userViewModel: UserViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        userViewModel = UserViewModel(UserRepository())
        userViewModel.model.observe(this, Observer(::updateUI))
        btnSave.setOnClickListener {
            userViewModel.doSaveUser(editTextName.text.toString(),lastname.text.toString(),email.text.toString())
        }
    }
    private fun updateUI(model: UserViewModel.UIModel?) {
        if ( model is UserViewModel.UIModel.Loading) {
            progressBar.visibility = View.VISIBLE
            group.visibility = View.GONE
        } else{
            progressBar.visibility = View.GONE
            group.visibility = View.VISIBLE
        }
        when ( model ) {
            is UserViewModel.UIModel.SaveUser -> validateSaveUser(model.success)
        }
    }

    private fun validateSaveUser( response: Boolean) {
        if(response){
            val builder = AlertDialog.Builder(this )
            builder.setTitle( "Éxito" )
            builder.setMessage( "Usuario guardado." )
            builder.show()
        }else{
            val builder = AlertDialog.Builder(this)
            builder.setTitle( "Error" )
            builder.setMessage( "Hubo un error, verifique los datos." )
            builder.show()
        }

    }
}
